require("dotenv").config();

import { Client, Intents } from "discord.js";

import parseEnv from "./libs/checkEnv";
const config = parseEnv();

let bot = new Client({
  intents: [Intents.FLAGS.GUILD_PRESENCES, Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS],
});

bot.on("guildMemberUpdate", async (partialMember) => {
  console.log("member update");
  let member = await partialMember.fetch();

  if (member.roles.cache.has(config.adultRole)) {
    console.log("already has adultRole");
    return;
  }
  console.log("checking roles");

  if (member.roles.cache.hasAny(...config.syncRoles)) {
    console.log("granting adultRole");
    try {
      await member.roles.add([config.adultRole]);
    } catch (error) {
      console.log("error granting role: bot prbly doesnt have permissions\n" + error);
    }
  }
});

bot.on("ready", async () => {
  console.log("fetching guilds...");
  await bot.guilds.fetch();
  console.log("ready!");
});

bot.login(process.env.DISCORD_TOKEN);
